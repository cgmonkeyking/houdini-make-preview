import toolutils
import os, os.path, sys, subprocess, re
from datetime import date

desktop = hou.ui.curDesktop()
scene_view = toolutils.sceneViewer()
viewport = scene_view.curViewport()
cameraName = str(viewport.camera())  

sceneName = hou.hscriptExpression("$HIPNAME")
sceneLocation = hou.hscriptExpression("$HIP")

print sceneName
print sceneLocation

today = str(date.today())
print(today) 

framesPath = sceneLocation + "/flip/" + sceneName + "_" + cameraName +"/"+ today
moviePath = sceneLocation + "/render/" + today
if not os.path.exists(moviePath):
    os.makedirs(moviePath)

if not os.path.exists(framesPath):
    os.makedirs(framesPath)    

print framesPath
print moviePath

base = sceneName+"_"+cameraName+"_$F4.jpg"
ffbase= sceneName+"_"+cameraName+"_%04d.jpg"

fullFramesPath = os.path.join(framesPath,base)

flipbook_options = scene_view.flipbookSettings().stash()
flipbook_options.frameRange(( hou.hscriptExpression("$FSTART"), hou.hscriptExpression("$FEND") ))
flipbook_options.output(fullFramesPath)
flipbook_options.useResolution(True)
flipbook_options.resolution((1920,1080))
flipbook_options.antialias(hou.flipbookAntialias.Good)

scene_view.flipbook(scene_view.curViewport(), flipbook_options)

ffexe = "C:\\ProgramData\\ffmpeg-20190502-7eba264-win64-static\\bin\\ffmpeg.exe"

ffmargs = " -f image2 -framerate "+str(hou.fps())
ffstart = hou.hscriptExpression("$FSTART")
ffinputseq = framesPath+"/"+ffbase

outfilename = str(moviePath)+ "/" + sceneName + "_" + ".mp4"
print outfilename

def checkfile(path):
     path      = os.path.expanduser(path)

     if not os.path.exists(path):
        return path

     root, ext = os.path.splitext(os.path.expanduser(path))
     dir       = os.path.dirname(root)
     fname     = os.path.basename(root)
     candidate = fname+ext
     index     = 0
     ls        = set(os.listdir(dir))
     while candidate in ls:
             candidate = "{}_{}{}".format(fname,index,ext)
             index    += 1
     return os.path.join(dir,candidate)
     
outfilename = checkfile(outfilename)


output = ffexe + ffmargs + " -start_number " + str(int(ffstart)) + " -i " +'"'+ ffinputseq +'"' + " " +'"'+outfilename+'"'
print output

p1 = subprocess.Popen(output)





